const request = require('supertest')
const app = require('../server.js')

describe('Convert Endpoint', () => {
  it('should success', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('preset', 'preset1')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(200);
      done();
  })

  // known format
  it('should success', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('preset', 'user')
      .field('format', 'avi')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(200);
      done();
  })

  // unknown format
  it('should return error', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('format', 'lala')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(400);
      expect(res.body.status).toEqual(-4);
      done();
  })

  // known compression
  it('should success', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('preset', 'user')
      .field('audioCodec', 'libmp3lame')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(200);
      done();
  })

  // unknown compression
  it('should return error', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('audioCodec', 'lala')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(400);
      expect(res.body.status).toEqual(-4);
      done();
  })

  // known videocodec
  it('should success', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('preset', 'user')
      .field('videoCodec', 'libx264')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(200);
      done();
  })

  // unknown videocodec
  it('should return error', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('videoCodec', 'lala')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(400);
      expect(res.body.status).toEqual(-4);
      done();
  })

  // known compression
  it('should success', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('preset', 'user')
      .field('compression', '40')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(200);
      done();
  })

  // unknown compression
  it('should return error', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('compression', 'lala')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(400);
      expect(res.body.status).toEqual(-1);
      done();
  })

  // unknown compression
  it('should return error', async (done) => {
    const res = await request(app)
      .post('/convert')
      .field('compression', '120')
      .attach('videoFile', './sample/sample.mp4');
      
      expect(res.statusCode).toEqual(400);
      expect(res.body.status).toEqual(-1);
      expect(res.body.message).toContain('compression out of range');
      done();
  })
})

