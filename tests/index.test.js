const request = require('supertest')
const app = require('../server.js')

describe('Home Endpoint', () => {
  it('should open home page', async (done) => {
    const res = await request(app).get('/');
    
    expect(res.statusCode).toEqual(200);
    done();
  })
})