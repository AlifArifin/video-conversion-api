require('dotenv').config({path: __dirname + '/.env'});

const express     = require('express');
const app         = express();
const fileUpload  = require('express-fileupload');

app.use(express.static('static'));

app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : '/tmp/'
}));
app.use(require('./routes'));

app.get('/', function(_, res) {
  res.sendFile('static/html/index.html', { root: __dirname });
});

module.exports = app;