require('dotenv').config({path: __dirname + '/.env'});

const port  = process.env.PORT || 3000;
const app   = require('./server')

module.exports = () => {
  app.listen(port, function() {
    console.log('listening on port', port);
  });
}