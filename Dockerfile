FROM node:alpine
RUN apk add --no-cache ffmpeg
WORKDIR /usr/src/app
COPY package.json ./
RUN yarn install
COPY . .
EXPOSE 3000
CMD ["node", "index.js"]