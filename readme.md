
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Video Converter Web API</h3>

  <p align="center">
    Prosa.ai Problem 2 Tasks 2
  <p>
</p>

<!-- ABOUT THE PROJECT -->
## Note

Sorry if my project doesn't meet requirement, I clarify the requirement today (20/8) but since today is holiday so I cannot expect any answer. Thus, my solution is based on my assumption.

## About The Project

Simple web application for video converting. 


### Built With

* [NodeJS](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [Docker](https://docker.com/)
* [fluent-ffmpeg](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg)
* [FFMpeg](https://ffmpeg.org/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* yarn; access this url [Yarn Installation](https://classic.yarnpkg.com/en/docs/install) for detail information
* docker; access this url [Docker Installation](https://docs.docker.com/get-docker/) for detail information
* ffmpeg; access this url [FFMpeg Installation](https://ffmpeg.org/download.html) for detail information

### Installation
 
1. Clone the repo
```sh
git clone https://gitlab.com/AlifArifin/simple-chatbot.git
```

2. Install Yarn packages; if you want to run in docker, you do not have to do this step
```sh
yarn install
```

### Run
#### Local environment
1. Start the project
```sh
node index.js
```
2. You can access this project in `localhost:3000`. If you change the port, just adjust it.
#### Using Docker
1. Build image
```sh
docker build -t <your username>/<project name> .
```
2. Run the image; if you want to use another port, just change it.
```sh
docker run -p 3000:3000 -d <your username>/<project name>
```
3. You can access this project in `localhost:3000`. If you change the port, just adjust it.

### Test
#### Local environment
1. Use this command
```sh
yarn test
```
#### Using docker
1. Build image. Please take a note that this Dockerfile use previous image.
```sh
docker build -t <your username>/<project name>-test . -f tests/Dockerfile
```
2. Run the image
```sh
docker run <your username>/<project name>-test
```
## Endpoints
```
POST /convert
-H Content-Type multipart/form-data
```
payload
```
files movieFiles
text  presets
text  format
text  audioCodec
text  videoCodec
text  compression
```
If `format`/`audioCodec`/`videoCodec`/`compression` is not defined, it will change to default. The default value can be accessed in `config/presets.json`.