const express = require('express');
const router  = express.Router();

router.use('/convert', require("./convert"));

module.exports = router;