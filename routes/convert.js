const express = require('express');
const router  = express.Router();
const ffmpeg  = require('fluent-ffmpeg');
const async   = require('async');
const shortid = require('shortid');
const presets = require('../config/presets.json');

const defaultFormat       = presets['default'].format || 'mp4';
const defaultAudioCodec   = presets['default'].audioCodec || 'libmp3lame';
const defaultVideoCodec   = presets['default'].videoCodec || 'libx264';
const defaultCompression  = presets['default'].compression || 80;

const basePath    = __dirname + '/..'
const uploadPath  = '/static/video/upload/'
const resultPath  = '/static/video/result/'

router.route("/")
  .post((req, res) => {
    let status = -1;
    let httpStatus = 400;
    let files = req.files;
    let preset = req.body.preset;
    let format = req.body.format;
    let audioCodec = req.body.audioCodec;
    let videoCodec = req.body.videoCodec;
    let compression = req.body.compression;

    if (files === undefined || Object.keys(files).length === 0) {
      return res.status(httpStatus).json({status, message: 'no files uploaded'});
    }

    if (preset === undefined || preset === '' || typeof(preset) !== 'string') {
      preset = "user";
    }

    if (format === undefined || format === '' || typeof(format) !== 'string') {
      format = defaultFormat;
    }

    if (audioCodec === undefined || audioCodec === '' || typeof(audioCodec) !== 'string') {
      audioCodec = defaultAudioCodec;
    }

    if (videoCodec === undefined || videoCodec === '' || typeof(videoCodec) !== 'string') {
      videoCodec = defaultVideoCodec;
    }

    if (compression === undefined || compression === '') {
      compression = defaultCompression;
    } else if (isNaN(compression)) {
      return res.status(httpStatus).json({status, message: 'compression invalid'});
    } else {
      compression = parseInt(compression);

      if (compression <= 0 || compression > 100) {
        return res.status(httpStatus).json({status, message: 'compression out of range'});
      }
    }

    if (preset !== "user") {
      if (presets[preset] !== undefined) {
        format = presets[preset].format;
        audioCodec = presets[preset].audioCodec;
        videoCodec = presets[preset].videoCodec;
        compression = presets[preset].compression;
      }
    }

    async.waterfall([
      (cb) => {
        let videoFile = files.videoFile;

        // extract format file
        let re = /(?:\.([^.]+))?$/;
        const formatFile = re.exec(videoFile.name)[1];
        let filename = shortid.generate();

        let fileDir = [uploadPath, filename, '.', formatFile].join('')
        let newFileDir = [resultPath, filename, '.', format].join('')

        videoFile.mv(basePath + fileDir, function(error) {
          if (error) {
            status = -2
            console.log('an error occurred: ', error.message, error);
            return cb('cannot save file', error.message);
          }
          
          cb(null, fileDir, newFileDir)
        })
      },
      (fileDir, newFileDir, cb) => {
        ffmpeg.ffprobe(basePath + fileDir, function(error, metadata) {
          if (error) {
            status = -3
            console.log('an error occurred: ', error.message, error);
            return cb('cannot read file', error.message);
          }

          let bitRate = metadata.format.bit_rate;
          let newBitRate = Math.floor(compression * bitRate / 100 / 1000);

          cb(null, fileDir, newFileDir, newBitRate)
        });
      },
      (fileDir, newFileDir, newBitRate, cb) => {
        ffmpeg(basePath + fileDir)
          .audioCodec(audioCodec)
          .videoCodec(videoCodec)
          .format(format)
          .videoBitrate(newBitRate)
          .output(basePath + newFileDir)
            .on('error', (error, stdout, stderr) => {
              status = -4
              console.log('an error occurred: ' + error.message, error, stderr);
              return cb('cannot convert file', error.message);
            })
            .on('end', () => {
              status = 1
              console.log('finished processing!')
              return res.download(basePath + newFileDir);
            })
          .run()
          }
    ], (message, data) => {
      return res.status(httpStatus).json({status, message, data});
    })
  })

module.exports = router;